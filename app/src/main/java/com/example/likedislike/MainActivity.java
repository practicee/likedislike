package com.example.likedislike;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private Button likeButton;
    private Button dislikeButton;
    private int currentImageIndex = 1; // Начнем с первой картинки
    private String[] categories = {"cat", "car", "gun", "flowers"};
    private int currentCategoryIndex = 0;
    private List<CategoryStatistics> categoryStatisticsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Находим элементы интерфейса по их ID
        imageView = findViewById(R.id.imageView);
        likeButton = findViewById(R.id.likeButton);
        dislikeButton = findViewById(R.id.dislikeButton);

        // Создаем объекты CategoryStatistics для каждой категории
        for (String category : categories) {
            categoryStatisticsList.add(new CategoryStatistics(category));
        }

        // Устанавливаем обработчики событий для кнопок "Лайк" и "Дизлайк"
        likeButton.setOnClickListener(v -> {
            // Увеличиваем индекс для следующего изображения
            currentImageIndex++;

            // Увеличиваем счетчик лайков для текущей категории
            CategoryStatistics currentCategoryStats = categoryStatisticsList.get(currentCategoryIndex);
            currentCategoryStats.incrementLikes();

            showNextImage();
        });

        dislikeButton.setOnClickListener(v -> {
            // Увеличиваем индекс для следующего изображения
            currentImageIndex++;

            // Увеличиваем счетчик дизлайков для текущей категории
            CategoryStatistics currentCategoryStats = categoryStatisticsList.get(currentCategoryIndex);
            currentCategoryStats.incrementDislikes();

            showNextImage();
        });

        // Первоначальное отображение первого изображения
        imageView.setImageResource(R.drawable.cat_image1);
    }

    private void showNextImage() {
        // Составляем имя файла изображения на основе текущей категории и индекса
        String currentCategory = categories[currentCategoryIndex];
        String imageName = currentCategory + "_image" + currentImageIndex;

        // Получаем ресурс изображения по его имени
        int imageResourceId = getResources().getIdentifier(imageName, "drawable", getPackageName());

        // Проверяем, существует ли такой ресурс
        if (imageResourceId != 0) {
            imageView.setImageResource(imageResourceId);
        } else {
            currentCategoryIndex++;
            if (currentCategoryIndex >= categories.length) {
                goToStatisticsScreen();
            } else {
                // Показываем следующее изображение в новой категории
                currentImageIndex = 1; // Сбрасываем индекс изображения
                showNextImage();
            }
        }
    }

    // Метод для перехода к экрану статистики
    private void goToStatisticsScreen() {
        Intent intent = new Intent(this, StatisticsActivity.class);

        // Создаем списки для передачи статистики по категориям
        ArrayList<String> categoryNames = new ArrayList<>();
        ArrayList<Integer> categoryLikes = new ArrayList<>();
        ArrayList<Integer> categoryDislikes = new ArrayList<>();

        for (CategoryStatistics categoryStatistics : categoryStatisticsList) {
            categoryNames.add(categoryStatistics.getCategory());
            categoryLikes.add(categoryStatistics.getLikes());
            categoryDislikes.add(categoryStatistics.getDislikes());
        }

        intent.putStringArrayListExtra("categoryNames", categoryNames);
        intent.putIntegerArrayListExtra("categoryLikes", categoryLikes);
        intent.putIntegerArrayListExtra("categoryDislikes", categoryDislikes);

        startActivity(intent);
    }

    // Внутренний класс для хранения статистики по категориям
    public class CategoryStatistics {
        private String category;
        private int likes;
        private int dislikes;

        public CategoryStatistics(String category) {
            this.category = category;
            this.likes = 0;
            this.dislikes = 0;
        }

        public String getCategory() {
            return category;
        }

        public int getLikes() {
            return likes;
        }

        public void incrementLikes() {
            likes++;
        }

        public int getDislikes() {
            return dislikes;
        }

        public void incrementDislikes() {
            dislikes++;
        }
    }
}

package com.example.likedislike;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;

public class StatisticsActivity extends AppCompatActivity {

    private TextView textViewCats;
    private TextView textViewCars;
    private TextView textViewGuns;
    private TextView textViewFlowers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        // Найдем элементы интерфейса
        textViewCats = findViewById(R.id.textViewCats);
        textViewCars = findViewById(R.id.textViewCars);
        textViewGuns = findViewById(R.id.textViewGuns);
        textViewFlowers = findViewById(R.id.textViewFlowers);

        // Получим данные о категориях, лайках и дизлайках из Intent
        Intent intent = getIntent();
        ArrayList<String> categoryNames = intent.getStringArrayListExtra("categoryNames");
        ArrayList<Integer> categoryLikes = intent.getIntegerArrayListExtra("categoryLikes");
        ArrayList<Integer> categoryDislikes = intent.getIntegerArrayListExtra("categoryDislikes");

        // Настроим текстовые поля для отображения статистики для каждой категории
        for (int i = 0; i < categoryNames.size(); i++) {
            String currentCategoryName = categoryNames.get(i);
            int likesCount = categoryLikes.get(i);
            int dislikesCount = categoryDislikes.get(i);

            // В зависимости от имени категории настраиваем соответствующее текстовое поле
            if ("cat".equals(currentCategoryName)) {
                textViewCats.setText(currentCategoryName + ": " + likesCount + " Likes, " + dislikesCount + " Dislikes");
            } else if ("car".equals(currentCategoryName)) {
                textViewCars.setText(currentCategoryName + ": " + likesCount + " Likes, " + dislikesCount + " Dislikes");
            } else if ("gun".equals(currentCategoryName)) {
                textViewGuns.setText(currentCategoryName + ": " + likesCount + " Likes, " + dislikesCount + " Dislikes");
            } else if ("flowers".equals(currentCategoryName)) {
                textViewFlowers.setText(currentCategoryName + ": " + likesCount + " Likes, " + dislikesCount + " Dislikes");
            }
        }
    }
}

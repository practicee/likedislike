package com.example.likedislike;

public class ImageItem {
    private String category; // Добавьте поле для категории
    private int imageResourceId;
    private boolean liked;

    public ImageItem(int imageResourceId, String category) {
        this.imageResourceId = imageResourceId;
        this.category = category; // Устанавливаем категорию при создании объекта
        this.liked = false;
    }

    public String getCategory() {
        return category;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }
}
